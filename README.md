# GraphQL + React App

Example of using GraphQL API subscriptions via Redis to expose realtime data in a react web app.

The solution is split into three standalone components that can be deployed independently:

- **`graphql-server`**: a GraphQL API exposing realtime data via subscriptions
- **`web-client`**: a React web app to access and display realtime data
- **`dotnet-publish`**: a dotnetcore console app to simulate device data by publishing one reading per second (from a CSV file) to the GraphQL API

All code is written in Typescript in VS Code apart from the dotnetcore demo app. All components support step through debugging and have a good developer experience.

## Suggested Runtime

- NodeJS for the GraphQL API and web app hosting
- Redis or other pub-sub provider for the GraphQL API

## Developer Prerequisites

- **Install [NodeJS](https://nodejs.org/en/)**
- **Install .NET Core 2.1 / Visual Studio 2017**
- **Install [VS Code](https://code.visualstudio.com/)**

## Running the Solution

- Start Redis: `redis\redis-server.exe`
- Run the GraphQL Server: - Open a command prompt inside `graphql-server` - Run `npm install` - Run `npm start`
- Run the reading publish demo: - Open a command prompt inside `dotnet-publish\src\PublishDemo` - Run `dotnet run --file data/PM_T14_161222_083749.csv --url http://localhost:3000`
- Run the web-client: - Open a command prompt inside `web-client` - Run `npm install` - Run `npm start`

## GraphQL API

- [Apollo Server 2](https://www.apollographql.com/docs/apollo-server/v2/api/apollo-server.html)
- [graphql-redis-subscriptions](https://github.com/davidyaha/graphql-redis-subscriptions)
- [TypeORM](http://typeorm.io)

## React Web App

- [create-react-app](https://github.com/facebook/create-react-app)
- [create-react-app-with-typescript](https://github.com/mui-org/material-ui/tree/master/examples/create-react-app-with-typescript/src)
- [Material UI](https://material-ui.com/)
- [Apollo Client](https://www.apollographql.com/docs/react/)
- [Chart,js](https://www.chartjs.org/)

## .NET Core App

- .Net Core 2.1
- [.NET Generic Host](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-2.2)
- [CSV Helper](https://joshclose.github.io/CsvHelper/)
- [HttpClientFactory](https://www.stevejgordon.co.uk/introduction-to-httpclientfactory-aspnetcore)
- [Transient Fault Handling with Polly](https://www.stevejgordon.co.uk/httpclientfactory-using-polly-for-transient-fault-handling)
