import config from 'config';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Container, interfaces } from 'inversify';
import { RedisOptions } from 'ioredis';
import 'reflect-metadata';
import { Connection } from 'typeorm';
import { connect } from './data/device/connection';
import { DeviceService } from './services/DeviceService';
import { TYPES } from './TYPES';

export const container = new Container();

container.bind<interfaces.Provider<Connection>>(TYPES.DeviceConnectionProvider).toProvider<Connection>(context => {
    return () => connect();
});

const redisOptions = config.get<RedisOptions>('redis.connection');

container.bind<RedisPubSub>(TYPES.RedisPubSub).toConstantValue(new RedisPubSub({ connection: redisOptions }));

container
    .bind<DeviceService>(TYPES.DeviceService)
    .to(DeviceService)
    .inSingletonScope();
