export const TYPES = {
    RedisPubSub: Symbol('RedisPubSub'),
    DeviceConnectionProvider: Symbol('DeviceConnectionProvider'),
    DeviceService: Symbol('DeviceService'),
};
