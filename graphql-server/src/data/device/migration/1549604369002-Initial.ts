import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1549604369002 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "Devices" ("Id" varchar PRIMARY KEY NOT NULL, "Name" varchar NOT NULL, "Model" varchar, CONSTRAINT "UQ_13c3e5430f62dddf5c1622bda54" UNIQUE ("Name"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "Devices"`);
    }

}
