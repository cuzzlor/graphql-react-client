import { MigrationInterface, QueryRunner } from 'typeorm';

export class DeviceAddLastActive1549604709350 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `CREATE TABLE "temporary_Devices" ("Id" varchar PRIMARY KEY NOT NULL, "Name" varchar NOT NULL, "Model" varchar, "LastActive" datetime, CONSTRAINT "UQ_13c3e5430f62dddf5c1622bda54" UNIQUE ("Name"))`,
        );
        await queryRunner.query(
            `INSERT INTO "temporary_Devices"("Id", "Name", "Model") SELECT "Id", "Name", "Model" FROM "Devices"`,
        );
        await queryRunner.query(`DROP TABLE "Devices"`);
        await queryRunner.query(`ALTER TABLE "temporary_Devices" RENAME TO "Devices"`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "Devices" RENAME TO "temporary_Devices"`);
        await queryRunner.query(
            `CREATE TABLE "Devices" ("Id" varchar PRIMARY KEY NOT NULL, "Name" varchar NOT NULL, "Model" varchar, CONSTRAINT "UQ_13c3e5430f62dddf5c1622bda54" UNIQUE ("Name"))`,
        );
        await queryRunner.query(
            `INSERT INTO "Devices"("Id", "Name", "Model") SELECT "Id", "Name", "Model" FROM "temporary_Devices"`,
        );
        await queryRunner.query(`DROP TABLE "temporary_Devices"`);
    }
}
