import config from 'config';
import { ConnectionOptions, createConnection } from 'typeorm';

export const connect = async () => {
    const opts = config.get<ConnectionOptions>('device-data.connection');
    return createConnection({ ...opts });
};
