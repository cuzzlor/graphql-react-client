import { RedisPubSub } from 'graphql-redis-subscriptions';
import { EntitySubscriberInterface, EventSubscriber, InsertEvent, RemoveEvent, UpdateEvent } from 'typeorm';
import { container } from '../../../container.config';
import { topics } from '../../../topics';
import { TYPES } from '../../../TYPES';
import { Device } from '../entity/Device';

@EventSubscriber()
export class DeviceSubscriber implements EntitySubscriberInterface<Device> {
    public listenTo() {
        return Device;
    }

    public afterInsert(event: InsertEvent<Device>) {
        container.get<RedisPubSub>(TYPES.RedisPubSub).publish(topics.created(Device), { id: event.entity.id });
    }

    public afterUpdate(event: UpdateEvent<Device>) {
        container.get<RedisPubSub>(TYPES.RedisPubSub).publish(topics.updated(Device), { id: event.entity.id });
    }

    public afterRemove(event: RemoveEvent<Device>) {
        container.get<RedisPubSub>(TYPES.RedisPubSub).publish(topics.deleted(Device), event.entity);
    }
}
