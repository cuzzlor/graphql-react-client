import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('Devices')
export class Device {
    @PrimaryGeneratedColumn('uuid', { name: 'Id' })
    public id!: string;

    @Column({ name: 'Name', unique: true })
    public name: string;

    @Column({ name: 'Model', nullable: true })
    public model?: string;

    @Column({ name: 'LastActive', nullable: true })
    public lastActive?: Date;

    constructor(name: string, model?: string, lastActive?: Date) {
        this.name = name;
        this.model = model;
        this.lastActive = lastActive;
    }
}
