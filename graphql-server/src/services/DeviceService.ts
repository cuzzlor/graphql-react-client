import { RedisPubSub } from 'graphql-redis-subscriptions';
import { inject, injectable } from 'inversify';
import { Connection, EntitySchema, FindManyOptions, ObjectType, Repository } from 'typeorm';
import uuid from 'uuid';
import { container } from '../container.config';
import { Device } from '../data/device/entity/Device';
import { lock } from '../lock-config';
import { DeviceInput } from '../model/DeviceInput';
import { ReadingInput } from '../model/ReadingInput';
import { topics } from '../topics';
import { TYPES } from '../TYPES';
@injectable()
export class DeviceService {
    private readonly lockKey = uuid();
    private readonly connectionProvider: () => Promise<Connection>;
    private connection?: Connection;

    constructor(
        @inject(TYPES.DeviceConnectionProvider)
        connectionProvider: () => Promise<Connection>,
    ) {
        this.connectionProvider = connectionProvider;
    }

    public async createDevice(deviceInput: DeviceInput): Promise<Device> {
        const repo = await this.getRepository(Device);
        return repo.save(new Device(deviceInput.name, deviceInput.model));
    }

    public async findDeviceById(id: string): Promise<Device | undefined> {
        const repo = await this.getRepository(Device);
        return repo.findOne(id);
    }

    public async findDevice(args: Partial<Device>): Promise<Device | undefined> {
        const repo = await this.getRepository(Device);
        return repo.findOne(args);
    }

    public async deleteDevice(id: string): Promise<Partial<Device>> {
        const repo = await this.getRepository(Device);
        const entity = await repo.findOneOrFail(id);
        await repo.remove(entity);
        return entity;
    }

    public async findDevices(args?: { skip?: number; take?: number }): Promise<Device[]> {
        const repo = await this.getRepository(Device);
        const options: FindManyOptions<Device> = args || {};
        options.order = { name: 'ASC' };
        return repo.find(options);
    }

    public async publishReading(reading: ReadingInput): Promise<ReadingInput> {
        await container.get<RedisPubSub>(TYPES.RedisPubSub).publish(topics.received('Reading'), reading);
        return reading;
    }

    private async getRepository<Entity>(
        target: ObjectType<Entity> | EntitySchema<Entity> | string,
    ): Promise<Repository<Entity>> {
        const connection = await this.getConnection();
        return connection.getRepository(target);
    }

    private async getConnection(): Promise<Connection> {
        if (this.connection) {
            return this.connection;
        }
        return lock.acquire(this.lockKey, async () => {
            if (this.connection) {
                return this.connection;
            }
            this.connection = await this.connectionProvider();
            return this.connection;
        });
    }
}
