import { withFilter } from 'apollo-server';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { container } from '../container.config';
import { Device } from '../data/device/entity/Device';
import { DeviceInput } from '../model/DeviceInput';
import { ReadingInput } from '../model/ReadingInput';
import { DeviceService } from '../services/DeviceService';
import { topics } from '../topics';
import { TYPES } from '../TYPES';

export const resolvers = {
    Query: {
        device: async (source: any, args: { id: string; name: string }) =>
            container.get<DeviceService>(TYPES.DeviceService).findDevice(args),
        devices: async (source: any, args: { skip?: number; take?: number }) =>
            container.get<DeviceService>(TYPES.DeviceService).findDevices(args),
    },

    Mutation: {
        createDevice: async (source: any, { device }: { device: DeviceInput }) =>
            container.get<DeviceService>(TYPES.DeviceService).createDevice(device),
        deleteDevice: async (source: any, { id }: { id: string }) =>
            container.get<DeviceService>(TYPES.DeviceService).deleteDevice(id),
        publishReading: async (source: any, { reading }: { reading: ReadingInput }) =>
            container.get<DeviceService>(TYPES.DeviceService).publishReading(reading),
    },

    Subscription: {
        deviceCreated: {
            subscribe: () => container.get<RedisPubSub>(TYPES.RedisPubSub).asyncIterator(topics.created(Device)),
            resolve: ({ id }: { id: string }) => container.get<DeviceService>(TYPES.DeviceService).findDeviceById(id),
        },
        deviceDeleted: {
            subscribe: () => container.get<RedisPubSub>(TYPES.RedisPubSub).asyncIterator(topics.deleted(Device)),
            resolve: (device: Partial<Device>) => device,
        },
        readingReceived: {
            subscribe: withFilter(
                (_: any, args: any) =>
                    container.get<RedisPubSub>(TYPES.RedisPubSub).asyncIterator(topics.received('Reading')),
                (payload: ReadingInput, args: { deviceId: string }) => payload.deviceId === args.deviceId,
            ),
            resolve: (reading: ReadingInput) => reading,
        },
    },

    Reading: {
        device: async (reading: ReadingInput) =>
            container.get<DeviceService>(TYPES.DeviceService).findDeviceById(reading.deviceId),
    },
};
