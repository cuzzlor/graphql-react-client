import { ObjectType } from 'typeorm';
import { isString } from 'util';

const buildTopic = <T>(target: ObjectType<T> | string, label: string, id?: any): string => {
    const targetValue = isString(target) ? target : target.name;
    let topic = `${targetValue}_${label}`;
    if (id) {
        topic += `.${id}`;
    }
    return topic;
};

export const topics = {
    created: <T>(target: ObjectType<T> | string) => buildTopic(target, 'Created'),
    updated: <T>(target: ObjectType<T> | string, id?: any) => buildTopic(target, 'Updated', id),
    deleted: <T>(target: ObjectType<T> | string, id?: any) => buildTopic(target, 'Deleted', id),
    received: <T>(target: ObjectType<T> | string, id?: any) => buildTopic(target, 'Received', id),
};
