export interface ReadingInput {
    deviceId: string;
    time: string;

    limResistance: number;
    hpuResistance: number;
    instVoltage: number;
    hpuVoltage: number;
    hpuCurrent: number;
    hpuApparentPower: number;
    hpuActivePower: number;
    hpuFrequency: number;
    instTemperature: number;
    hpuTemperature: number;
    cabinetTemperature: number;

    tmsLimResistance: number;
    tmsHpuResistance: number;
    tmsInstVoltage: number;
    tmsHpuVoltage: number;
    tmsHpuCurrent: number;
    tmsHpuApparentPower: number;
    tmsHpuActivePower: number;
    tmsHpuFrequency: number;
    tmsInstTemperature: number;
    tmsHpuTemperature: number;
    tmsCabinetTemperature: number;
}
