export interface DeviceInput {
    name: string;
    model?: string;
}
