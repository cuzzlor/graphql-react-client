import { ApolloServer } from 'apollo-server';
import config from 'config';
import * as fs from 'fs';
import * as path from 'path';
import { resolvers } from './resolvers/resolvers';

const typeDefs = fs.readFileSync(path.join(__dirname, 'schema/schema.graphql'), 'utf8');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: config.get<boolean>('graphql.introspection'),
    playground: config.get('graphql.playground'),
    debug: config.get<boolean>('graphql.debug'),
});

server.listen({ port: process.env.PORT }).then(({ url }) => {
    // tslint:disable-next-line:no-console
    console.log(`🚀 Server ready at ${url}`);
});
