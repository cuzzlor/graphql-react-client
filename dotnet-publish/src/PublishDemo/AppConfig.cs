﻿using System;
using System.Collections.Generic;
using System.Text;

namespace demo
{
    class AppConfig
    {
        public string DataFilePath { get; set; }
        public string GraphQLUrl { get; set; }
    }
}
