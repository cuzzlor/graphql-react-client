﻿using System;
using DeviceData.Csv;
using GraphQL.Client.Http;
using GraphQL.Common.Request;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using GraphQL.Client;

namespace demo
{
    class PublishReadingFromCsvDemoService : IHostedService
    {
        private readonly AppConfig _config;
        private readonly ILogger<PublishReadingFromCsvDemoService> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public PublishReadingFromCsvDemoService(IOptions<AppConfig> config, ILogger<PublishReadingFromCsvDemoService> logger, IHttpClientFactory httpClientFactory)
        {
            _config = config.Value;
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var fullPath = Path.GetFullPath(_config.DataFilePath);

            _logger.LogInformation("Opening data file: {dataFile}", fullPath);

            using (var reader = File.OpenText(fullPath))
            using (var client = _httpClientFactory.CreateClient("GraphQL").AsGraphQLClient(new GraphQLHttpClientOptions { EndPoint = new Uri(_config.GraphQLUrl) }))
            {
                var csvReader = new ReadingCsvReader(reader);

                string deviceId = null;

                foreach (var reading in csvReader.Read())
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        await Task.FromCanceled(cancellationToken);
                    }

                    if (deviceId == null)
                    {
                        deviceId = await FindOrRegisterDevice(csvReader.DeviceName, client, cancellationToken);

                        if (cancellationToken.IsCancellationRequested)
                        {
                            await Task.FromCanceled(cancellationToken);
                        }
                    }

                    reading.DeviceId = deviceId;

                    var response = await client.SendMutationAsync(new GraphQLRequest
                    {
                        Query = Query.PublishReading,
                        Variables = new { reading }
                    }, cancellationToken);

                    _logger.LogInformation("Reading processed: {time}", (string)response.Data.publishReading.time);

                    await Task.Delay(1000, cancellationToken);
                }
            }
        }

        private static async Task<string> FindOrRegisterDevice(
            string deviceName, 
            GraphQLHttpClient client,
            CancellationToken cancellationToken)
        {
            var findDeviceResponse = await client.SendQueryAsync(new GraphQLRequest
            {
                Query = Query.FindDeviceByName,
                Variables = new {name = deviceName }
            }, cancellationToken);

            if (findDeviceResponse.Data.device != null)
            {
                return findDeviceResponse.Data.device.id;
            }

            var registerDeviceResponse = await client.SendQueryAsync(new GraphQLRequest
            {
                Query = Query.RegisterDevice,
                Variables = new { device = new { name = deviceName } }
            }, cancellationToken);

            return registerDeviceResponse.Data.createDevice.id;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
        }
    }
}
