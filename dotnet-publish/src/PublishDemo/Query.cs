﻿using System;
using System.Collections.Generic;
using System.Text;

namespace demo
{
    public static class Query
    {
        public static readonly string FindDeviceByName = @"
            query FindDeviceByName($name: String!) {
              device(name: $name) {
                id
                name
                model
              }
            }";

        public static readonly string RegisterDevice = @"
            mutation RegisterDevice($device: DeviceInput!) {
              createDevice(device: $device) {
                id
                name
                model
              }
            }";

        public static readonly string PublishReading = @"
            mutation PublishReading($reading: ReadingInput!) {
              publishReading(reading: $reading) {
                time
              }
            }";
    }
}
