﻿using System;

namespace DeviceData.Model
{
    public class Reading
    {
        public string DeviceId { get; set; }

        public DateTimeOffset Time { get; set; }

        public int LimResistance { get; set; }
        public int HpuResistance { get; set; }
        public int InstVoltage { get; set; }
        public int HpuVoltage { get; set; }
        public int HpuCurrent { get; set; }
        public int HpuApparentPower { get; set; }
        public int HpuActivePower { get; set; }
        public decimal HpuFrequency { get; set; }
        public decimal InstTemperature { get; set; }
        public decimal HpuTemperature { get; set; }
        public decimal CabinetTemperature { get; set; }

        public int TmsLimResistance { get; set; }
        public int TmsHpuResistance { get; set; }
        public int TmsInstVoltage { get; set; }
        public int TmsHpuVoltage { get; set; }
        public int TmsHpuCurrent { get; set; }
        public int TmsHpuApparentPower { get; set; }
        public int TmsHpuActivePower { get; set; }
        public decimal TmsHpuFrequency { get; set; }
        public decimal TmsInstTemperature { get; set; }
        public decimal TmsHpuTemperature { get; set; }
        public decimal TmsCabinetTemperature { get; set; }
    }
}
