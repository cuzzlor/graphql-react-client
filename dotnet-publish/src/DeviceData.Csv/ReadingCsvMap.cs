﻿using System;
using System.Globalization;
using CsvHelper.Configuration;
using DeviceData.Model;

namespace DeviceData.Csv
{
    public sealed class ReadingCsvMap : ClassMap<Reading>
    {
        public ReadingCsvMap()
        {
            Map(r => r.DeviceId).Ignore();

            Map(r => r.Time).ConvertUsing(r => DateTimeOffset.ParseExact($"{r.GetField<string>("Date").Trim()} {r.GetField<string>(" Time").Trim()}", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));

            Map(r => r.LimResistance).Name(" ROV LIM Instrument (Ohms)");
            Map(r => r.HpuResistance).Name(" ROV LIM HPU (Ohms)");
            Map(r => r.InstVoltage).Name(" ROV Instrument Voltage (Volts)");
            Map(r => r.HpuVoltage).Name(" ROV HPU Voltage (Volts)");
            Map(r => r.HpuCurrent).Name(" ROV HPU Current (Amps)");
            Map(r => r.HpuApparentPower).Name(" ROV HPU Apparent Power (VA)");
            Map(r => r.HpuActivePower).Name(" ROV HPU Active Power (W)");
            Map(r => r.HpuFrequency).Name(" ROV HPU Frequency (Hertz)");
            Map(r => r.InstTemperature).Name(" ROV Instrument Temperature (Degrees C)");
            Map(r => r.HpuTemperature).Name(" ROV HPU Temperature (Degrees C)");
            Map(r => r.CabinetTemperature).Name(" ROV Cabinet Temperature (Degrees C)");

            Map(r => r.TmsLimResistance).Name(" TMS LIM Instrument (Ohms)");
            Map(r => r.TmsHpuResistance).Name(" TMS LIM HPU (Ohms)");
            Map(r => r.TmsInstVoltage).Name(" TMS Instrument Voltage (Volts)");
            Map(r => r.TmsHpuVoltage).Name(" TMS HPU Voltage (Volts)");
            Map(r => r.TmsHpuCurrent).Name(" TMS HPU Current (Amps)");
            Map(r => r.TmsHpuApparentPower).Name(" TMS HPU Apparent Power (VA)");
            Map(r => r.TmsHpuActivePower).Name(" TMS HPU Active Power (W)");
            Map(r => r.TmsHpuFrequency).Name(" TMS HPU Frequency (Hertz)");
            Map(r => r.TmsInstTemperature).Name(" TMS Instrument Temperature (Degrees C)");
            Map(r => r.TmsHpuTemperature).Name(" TMS HPU Temperature (Degrees C)");
            Map(r => r.TmsCabinetTemperature).Name(" TMS Cabinet Temperature (Degrees C)");

        }
    }
}
