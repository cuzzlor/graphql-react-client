﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using DeviceData.Model;

namespace DeviceData.Csv
{
    public class ReadingCsvReader
    {
        private readonly TextReader _reader;

        public string SoftwareInfo { get; private set; }
        public string DeviceName { get; private set; }

        public ReadingCsvReader(TextReader reader)
        {
            _reader = reader;
        }

        public IEnumerable<Reading> Read()
        {
            var csvReader = new CsvReader(_reader);

            csvReader.Configuration.RegisterClassMap<ReadingCsvMap>();
            csvReader.Configuration.MissingFieldFound = null;

            // first row has the software info
            csvReader.Read();
            SoftwareInfo = csvReader.GetField<string>(0);

            // second row has the device name
            csvReader.Read();
            DeviceName = csvReader.GetField<string>(0);

            // third row has the header
            csvReader.Read();
            csvReader.ReadHeader();

            // subsequent rows are all readings
            csvReader.Read();
            return csvReader.GetRecords<Reading>();
        }
    }
}
