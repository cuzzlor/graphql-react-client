import gql from "graphql-tag";

export const readingReceived = gql`
  subscription ReadingReceived($deviceId: String!) {
    readingReceived(deviceId: $deviceId) {
      time
      limResistance
      hpuResistance
      instVoltage
      hpuVoltage
      hpuCurrent
      hpuApparentPower
      hpuActivePower
      hpuFrequency
      instTemperature
      hpuTemperature
      cabinetTemperature
      tmsLimResistance
      tmsHpuResistance
      tmsInstVoltage
      tmsHpuVoltage
      tmsHpuCurrent
      tmsHpuApparentPower
      tmsHpuActivePower
      tmsHpuFrequency
      tmsInstTemperature
      tmsHpuTemperature
      tmsCabinetTemperature
    }
  }
`;
