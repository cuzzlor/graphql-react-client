import Paper from "@material-ui/core/Paper";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles
} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import * as moment from "moment";
import * as React from "react";
import { Reading } from "./Reading";
import { withRoot } from "./withRoot";

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: theme.spacing.unit * 3,
      overflowX: "auto"
    },
    table: {
      minWidth: 400
    },
    row: {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.background.default
      }
    }
  });

interface Props {
  readings?: Reading[];
}

class ReadingsTable extends React.Component<WithStyles<typeof styles> & Props> {
  public render() {
    const { classes } = this.props;
    const readings = this.props.readings || [];

    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <CustomTableCell>Time</CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                LIM Resistance
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                HPU Resistance
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                INST Voltage
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                HPU Voltage
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                HPU Current
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                HPU Apparent
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                HPU Active
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                HPU Frequency
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                INST Temperature
              </CustomTableCell>
              <CustomTableCell numeric={true} padding="dense">
                Cabinet Temperature
              </CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {readings.map(n => {
              return (
                <TableRow key={n.time}>
                  <CustomTableCell component="th" scope="row">
                    {moment(n.time).format("hh:mm:ss")}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.limResistance}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.hpuResistance}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.instVoltage}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.hpuVoltage}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.hpuCurrent}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.hpuApparentPower}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.hpuActivePower}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.hpuFrequency}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.instTemperature}
                  </CustomTableCell>
                  <CustomTableCell numeric={true} padding="dense">
                    {n.cabinetTemperature}
                  </CustomTableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default withRoot(withStyles(styles, { withTheme: true })(ReadingsTable));
