import { Device } from "./Device";

/**
 *
 *
 * @export
 * @interface Reading
 * ROV – Remotely Operated Vehicle or subsea robot
 * TMS – Tether Management System is the unit which gets deployed with the ROV with the tether wrapped onto a drum. The ROV undocks from this at depth with the tether rolled out as the ROV swims away.
 * LIM – Line Insulation Monitor an analog resistance measurement for certain wire conductors in the tether. This value should be high such as 6000000 ohms if the value is low there is a short or fault condition which will shut the system down. This value often comes under scrutiny as it indicates if damage to the tether occurred and can indicate that problems will develop if nothing is done.
 * Inst – Instrumentation power is the electrical circuit supplying power to electronics (cameras, lights control circuit).
 * HPU – Hydraulic Power Unit is the electrical power supplied to the hydraulic motor. The electrical power energises the motor which produces hydraulic pressure or power (thrusters or propulsion system and robot arm uses hydraulic power).
 */
export interface Reading {
  device: Device;
  time: string;

  limResistance: number;
  hpuResistance: number;
  instVoltage: number;
  hpuVoltage: number;
  hpuCurrent: number;
  hpuApparentPower: number;
  hpuActivePower: number;
  hpuFrequency: number;
  instTemperature: number;
  hpuTemperature: number;
  cabinetTemperature: number;

  tmsLimResistance: number;
  tmsHpuResistance: number;
  tmsInstVoltage: number;
  tmsHpuVoltage: number;
  tmsHpuCurrent: number;
  tmsHpuApparentPower: number;
  tmsHpuActivePower: number;
  tmsHpuFrequency: number;
  tmsInstTemperature: number;
  tmsHpuTemperature: number;
  tmsCabinetTemperature: number;
}
