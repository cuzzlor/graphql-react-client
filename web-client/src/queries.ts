import gql from "graphql-tag";

export const findDevices = gql`
  {
    devices {
      id
      name
      model
    }
  }
`;
