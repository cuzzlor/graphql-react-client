import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  WithTheme
} from "@material-ui/core/styles";
import * as _ from "lodash";
import * as moment from "moment";
import * as React from "react";
import { Line, LinearComponentProps } from "react-chartjs-2";
import { Reading } from "./Reading";
import { withRoot } from "./withRoot";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: theme.spacing.unit * 3,
      overflowX: "auto"
    },
    table: {
      minWidth: 400
    },
    row: {
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.background.default
      }
    }
  });

interface Props {
  readings: Reading[];
}

class ReadingsChart extends React.Component<
  WithTheme & WithStyles<typeof styles> & Props,
  LinearComponentProps
> {
  public state: LinearComponentProps = {
    data: {
      labels: [],
      datasets: [
        {
          type: "line",
          label: "HPU Apparent",
          borderWidth: 2,
          lineTension: 0.45,
          data: [],
          yAxisID: "y-temp-freq"
        },
        {
          type: "line",
          label: "HPU Active",
          borderWidth: 2,
          lineTension: 0.45,
          data: [],
          yAxisID: "y-temp-freq"
        },
        {
          type: "line",
          label: "INST Temperature",
          borderWidth: 2,
          lineTension: 0.45,
          data: [],
          yAxisID: "y-temp-freq"
        },
        {
          type: "line",
          label: "Cabinet Temperature",
          borderWidth: 2,
          lineTension: 0.45,
          data: [],
          yAxisID: "y-temp-freq"
        },
        {
          type: "line",
          label: "LIM Resistance",
          borderWidth: 2,
          lineTension: 0.45,
          data: [],
          yAxisID: "y-ohms"
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        enabled: true
      },
      scales: {
        xAxes: [
          {
            ticks: {
              autoSkip: true,
              maxTicksLimit: 100
            }
          }
        ],
        yAxes: [
          {
            type: "linear",
            display: true,
            position: "left",
            id: "y-temp-freq"
          },
          {
            type: "linear",
            display: true,
            position: "right",
            id: "y-ohms",
            gridLines: {
              drawOnChartArea: false
            },
            ticks: {
              beginAtZero: false,
              suggestedMin: 59000000,
              suggestedMax: 60500000
            }
          }
        ]
      }
    }
  };

  public componentWillReceiveProps(nextProps: Props) {
    if (nextProps.readings.length > 0) {
      this.setState({
        data: this.updateChartData(nextProps.readings)
      });
    }
  }

  public render() {
    return <Line data={this.state.data} options={this.state.options} />;
  }

  private updateChartData = (readings: Reading[]): Chart.ChartData => {
    const newReading = readings[readings.length - 1];
    const data = this.state.data as Chart.ChartData;

    const newDataSets = this.updateDataSets(newReading, [
      "hpuApparentPower",
      "hpuActivePower",
      "instTemperature",
      "cabinetTemperature",
      "limResistance"
    ]);

    return {
      ...this.state.data,
      datasets: newDataSets,
      labels: [
        ...(data.labels as string[]),
        moment(newReading.time).format("hh:mm:ss")
      ].slice(-100)
    };
  };

  private updateDataSets = (
    reading: Reading,
    keys: string[]
  ): Chart.ChartDataSets[] => {
    const data = this.state.data as Chart.ChartData;
    let index = 0;
    return keys.map(key => {
      const dataSet = data.datasets![index++];
      const newDataSet = { ...dataSet };
      newDataSet.data = [...dataSet.data!, reading[key]].slice(
        -100
      ) as number[];
      return newDataSet;
    });
  };
}

export default withRoot(withStyles(styles, { withTheme: true })(ReadingsChart));
