import {
  AppBar,
  Badge,
  Divider,
  Drawer,
  IconButton,
  List,
  Toolbar
} from "@material-ui/core";
import { Theme } from "@material-ui/core/styles/createMuiTheme";
import createStyles from "@material-ui/core/styles/createStyles";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import MenuIcon from "@material-ui/icons/Menu";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ApolloClient from "apollo-client";
import * as classNames from "classnames";
import * as React from "react";
import { ChildProps, graphql, withApollo } from "react-apollo";
import { Device } from "./Device";
import { mainListItems, secondaryListItems } from "./listItems";
import { findDevices } from "./queries";
import { Reading } from "./Reading";
import ReadingsChart from "./ReadingsChart";
import ReadingsTable from "./ReadingsTable";
import { readingReceived } from "./subscriptions";
import { withRoot } from "./withRoot";

const drawerWidth = 240;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: "flex"
    },
    toolbar: {
      paddingRight: 24 // keep right padding when drawer closed
    },
    toolbarIcon: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: "0 8px",
      ...theme.mixins.toolbar
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginLeft: 12,
      marginRight: 36
    },
    menuButtonHidden: {
      display: "none"
    },
    title: {
      flexGrow: 1
    },
    drawerPaper: {
      position: "relative",
      whiteSpace: "nowrap",
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    drawerPaperClose: {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      width: theme.spacing.unit * 7,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing.unit * 9
      }
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
      height: "100vh",
      overflow: "auto"
    },
    chartContainer: {
      height: 400
    },
    tableContainer: {
      height: 320
    },
    h5: {
      marginBottom: theme.spacing.unit * 2
    }
  });

interface State {
  open: boolean;
  chartReadings: Reading[];
  tableReadings: Reading[];
}

interface FindDevicesResponse {
  devices: Device[];
}

interface ReadingReceivedResponse {
  data: { readingReceived: Reading };
}

type Props = ChildProps<
  WithStyles<typeof styles> & { client: ApolloClient<{}> },
  FindDevicesResponse
>;

class App extends React.Component<Props, State> {
  private static getDevice = (props: Props): Device | undefined =>
    props.data && props.data.devices && props.data.devices.length > 0
      ? props.data.devices[0]
      : undefined;

  public state: State = {
    open: false,
    chartReadings: [],
    tableReadings: []
  };

  public handleDrawerOpen = () => {
    this.setState({
      open: true
    });
  };

  public handleDrawerClose = () => {
    this.setState({
      open: false
    });
  };

  public shouldComponentUpdate(nextProps: Props, nextState: State) {
    this.subscribeToDeviceReadings(nextProps);
    return true;
  }

  public subscribeToDeviceReadings = (nextProps: Props) => {
    const currentDevice = App.getDevice(this.props);
    const nextDevice = App.getDevice(nextProps);
    const hasNewDevice = !!nextDevice && nextDevice !== currentDevice;

    if (hasNewDevice) {
      this.props.client
        .subscribe<ReadingReceivedResponse>({
          query: readingReceived,
          variables: { deviceId: nextDevice!.id }
        })
        .subscribe(this.readingReceived);
    }
  };

  public readingReceived = (response: ReadingReceivedResponse) => {
    this.setState(prevState => ({
      chartReadings: [
        ...prevState.chartReadings,
        response.data.readingReceived
      ].slice(-100)
    }));

    this.setState(prevState => ({
      tableReadings: [
        ...prevState.tableReadings,
        response.data.readingReceived
      ].slice(-5)
    }));
  };

  public render() {
    const { classes } = this.props;
    const { loading, devices } = this.props.data!;
    const { open, chartReadings, tableReadings } = this.state;

    return (
      <div className={classes.root}>
        <AppBar
          position="absolute"
          className={classNames(classes.appBar, open && classes.appBarShift)}
        >
          <Toolbar disableGutters={!open} className={classes.toolbar}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(
                classes.menuButton,
                open && classes.menuButtonHidden
              )}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap={true}
              className={classes.title}
            >
              {loading
                ? "loading..."
                : devices && devices.length > 0
                ? devices[0].name
                : "!!! No Devices Registered !!!"}
            </Typography>
            <IconButton color="inherit">
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classNames(
              classes.drawerPaper,
              !open && classes.drawerPaperClose
            )
          }}
          open={open}
        >
          <div className={classes.toolbarIcon}>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>{mainListItems}</List>
          <Divider />
          <List>{secondaryListItems}</List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          {devices && devices.length > 0 && chartReadings.length > 0 ? (
            <div>
              <div className={classes.chartContainer}>
                <ReadingsChart readings={chartReadings} />
              </div>
              <div className={classes.tableContainer}>
                <ReadingsTable readings={tableReadings} />
              </div>
            </div>
          ) : (
            <Typography variant="h4" gutterBottom={true} component="h2">
              waiting for readings...
            </Typography>
          )}
        </main>
      </div>
    );
  }
}

const withDevices = graphql<{}, FindDevicesResponse, {}>(findDevices);

export default withRoot(
  withStyles(styles, { withTheme: true })(withDevices(withApollo(App)))
);
